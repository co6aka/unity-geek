﻿using UnityEngine;

// have three types of subclasses to increase distance:
//    distance ++;
public class Cucaracha : MonoBehaviour
{
    public bool vertical;

    protected long distance = 0;
    protected bool finished = false;
    Vector3 direction;

    private void Awake()
    {
        if (vertical)
            direction = transform.up;
        else
            direction = transform.right * -1f;
    }

    // TODO: move roach one step forward in the requested direction
    // via different methods

}
