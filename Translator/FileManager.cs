﻿using System;
using System.IO;
using System.Collections.Generic;

// sample app for Writing & Reading files
public class FileManager
{
    public static void Write(string fileName, string text)   // path + filename
    {
        // if a file doesn't exist, create it
        if (!File.Exists(fileName))
        {
            using (StreamWriter sw = File.CreateText(fileName))
            {
                sw.WriteLine(text);
            }
        }
        else
        {   // otherwise, append to existing file
            using (StreamWriter sw = File.AppendText(longName))
            {
                sw.WriteLine(text);
            }
        }
    }
    
    public static void Read(string fileName)
    {
        using (StreamReader sr = File.OpenText(fileName))
        {
            string s = "";
            while ((s = sr.ReadLine()) != null)
            {
                Console.WriteLine(s);
            }
        }
    }

    public static void Main()
    {
        string fileName = @"c:\temp\MyTest.txt";

        Console.WriteLine(">>> writing to " + fileName + " <<<");
        Write(fileName, "howdy");
        Write(fileName, "rocky road");

        Console.WriteLine(">>> reading from " + fileName + " <<<");
        Read(fileName);

        Console.WriteLine("\n Press Enter to quit");
        Console.ReadLine();     // wait for user to close the app
    }
    
}