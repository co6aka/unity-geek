﻿using UnityEngine;
using UnityEngine.UI;

public class Word : MonoBehaviour
{
    [HideInInspector]
    public static Letter stashed = null;

    private Letter[] letters;

    void Start()
    {
        letters = FindObjectsOfType<Letter>();  // grab all letters from game into a word
    }

    // user can toggle letters on and off
    public static void ApplySelection(Letter let, bool isHilited)
    {
        // stash, switch or un-stash the letter
        // depending on whether it's hilited or not
        // work out the flowchart first
        // ...
        // print(stashed, let)
        
    }

    static void Swap(Letter a, Letter b)
    {
        // exchange A & B's positions on screen
        // + clear stash and all selections
    }

}
