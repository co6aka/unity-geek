﻿using UnityEngine;
using UnityEngine.UI;

public class Alphabet : MonoBehaviour {

    public GameObject sampleLetter;
    public float left, right;

    public Text status;  // Game Over text

    void Start () {
        // Kick off letter generation
    }

    void GenerateLetter()
    {
        Vector2 startPos = new Vector2(0f, 0f);     // <- get random position between left and right

        GameObject letter = Instantiate(sampleLetter, startPos, Quaternion.identity, sampleLetter.transform.parent);
        letter.SetActive(true);  // make visible and interactable
    }

}
