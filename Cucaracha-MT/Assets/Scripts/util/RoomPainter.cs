﻿using UnityEngine;

public class RoomPainter : MonoBehaviour
{
    public GameObject image;
    public GameObject finish;
    private float startX, startY, endY, totalY;

    void Start()
    {
        startY = transform.position.y;      // our cockroach - Painter script attached to it
        startX = transform.position.x;
        endY = finish.transform.position.y;
        totalY = endY - startY;
        image.transform.position = transform.position;
    }

    void Update()
    {
        float yPos = (transform.position.y + startY)/2f;
        image.transform.position = new Vector2(transform.position.x, yPos);
        // TODO: replace the magic 35 number with a computed value?
        image.transform.localScale = new Vector2(1f, Mathf.Abs(yPos - startY)*35f);
    }
}
