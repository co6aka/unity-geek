﻿using UnityEngine;

public class Letter : MonoBehaviour {

    private SpriteRenderer sr;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // write a method to clear selection - paint the sprite in white color
    

    // select / deselect letter and notify word about it
    void OnMouseDown()
    {
        // select or deselect (paint in red or white)
        // 
        bool selected = false;
        // ..
        Word.ApplySelection(this, selected);  // notify Word that user selected / deselected a letter
    }

    // optionally, move letter up / down on U/D key clicks
    // to create multiple words
}
