﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// place objects in two corners, alternating up and down
public class Multiplier : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        CreateItem(prefab, 0f, 0f, 1.0f);
    }

    void CreateItem(GameObject prefab, float x, float y, float len)
    {
        Place(prefab, x, y, len);
        // parameters for other calls: (prefab, x+len*1.25f, y+len*1.25f, len/2f)
        // optinally, parameterize the above factors
        // try iterative and recursive approaches

    }

    ////////////////////////////////////////////////////////////////////////
    //////// Create an actual object and place it in the game //////////////
    ////////////////////////////////////////////////////////////////////////
    void Place(GameObject o, float x, float y, float scale)
    {
        Vector2 location = new Vector2(x, y);
        GameObject go = (GameObject)GameObject.Instantiate(o, location, Quaternion.identity);
        go.transform.localScale = new Vector2(scale, scale);
    }    

}
